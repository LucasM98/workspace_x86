/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT)
    {
        input = hw_LeerEntrada();

        if (input == SENSOR_2)
        {
            hw_alarma();
        }

        if (input == SENSOR_1)
        {
            hw_AbrirBarrera();

            do
            {
                input = hw_LeerEntrada();
            } while(input != SENSOR_2);
            
            hw_Pausems(5);
            hw_CerrarBarrera();
            
        }

    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
