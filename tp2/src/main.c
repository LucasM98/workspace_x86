/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include "maqEst.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input=0;
    uint16_t cont_ms=0;

    hw_Init();
    init_maqEst();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) 
    {
        input = hw_LeerEntrada();

        if(input == INGRESO_FICHA)
        {
            evIngreso_Ficha();
        }

        if(input == ELIGE_TE)
        {
            evElige_Te();
        }

        if(input == ELIGE_CAFE)
        {
            evElige_Cafe();
        }

        cont_ms++;
        if (cont_ms == 100)
        {
            cont_ms = 0;
            ev100ms();
        }

        estadoActual();
        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
