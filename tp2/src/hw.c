/*==================[inclusions]=============================================*/

#include "hw.h"
#include "maqEst.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;



/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t*1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void devolucionFicha(void)
{
    printf("FICHA DEVUELTA\n\n");
}

void erogarTe(void)
{
    printf("EREGANDO TE\n\n");
}

void erogarCafe(void)
{
    printf("EREGANDO CAFE\n\n");
}

void finTe(void)
{
    printf("FINALIZÓ LA EREGACIÓN DE TE\n\n");
}

void finCafe(void)
{
    printf("FINALIZÓ LA EREGACIÓN DE CAFE\n\n");
}

void activarAlarma(void)
{
    printf("ALARMA ACTIVADA\n\n");
}

void apagarAlarma(void)
{
    printf("ALARMA APAGADA\n\n");
}

/*==================[end of file]============================================*/
