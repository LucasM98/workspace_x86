#include "maqEst.h"
#include "hw.h"
#include <stdio.h>

#define FREC1_LED 50
#define TIEMPO_DECISION 300
#define TIEMPO_TE 300 //cant. de veces que se deben cumplir 100ms para llegar a 30seg
#define TIEMPO_CAFE 450 //cant. de veces que se deben cumplir 100ms para llegar a 45seg

uint8_t state;
uint8_t ficha;
uint8_t te;
uint8_t cafe;
uint8_t cont_tiempo;
uint8_t trans100ms;

void clearVariables(void)
{
    ficha = 0;
    te = 0;
    cafe = 0;
    trans100ms = 0;
}

void init_maqEst(void)
{
    state = REPOSO;
    cont_tiempo = 0;
    clearVariables();
}

void ev100ms(void)
{
    trans100ms=1;
}

void evIngreso_Ficha(void)
{
    ficha = 1;
}

void evElige_Te(void)
{
    te = 1;
}

void evElige_Cafe(void)
{
    cafe = 1;
}


void estadoActual(void)
{
    switch (state)
        {
        case REPOSO:
            if(trans100ms && (cont_tiempo<FREC1_LED))
            {
                cont_tiempo++;
            }
            else if(trans100ms && (cont_tiempo==FREC1_LED))
            {
                cont_tiempo=0;
                //toggleLED();
            }
            
            if(ficha==1)
            {
                printf("FICHA INGRESADA \n\n");
                printf("SELECCIONE INFUSIÓN\n\n");
                printf("-TE\n\n-CAFE\n\n");
                cont_tiempo=0;
                state = ESPERANDO_DECISION;
            }
            break;
        
        case ESPERANDO_DECISION:

            if(trans100ms)
            {
                if(cont_tiempo<TIEMPO_DECISION)
                {
                    cont_tiempo++;
                }
                else if(cont_tiempo==TIEMPO_DECISION)
                {
                    cont_tiempo=0;
                    ficha=0;
                    state = REPOSO;
                    devolucionFicha();
                }
            }

            else
            {
                if (te)
                {
                    erogarTe();
                    state = ESPERANDO_TE;
                    cont_tiempo=0;
                }
                else if (cafe)
                {
                    erogarCafe();
                    state = ESPERANDO_CAFE;
                    cont_tiempo=0;
                }
            }
            break;
        
        case ESPERANDO_TE:
            if(trans100ms && (cont_tiempo<TIEMPO_TE))
                {
                    //toggleLED();
                    cont_tiempo++;   
                }
            else if(trans100ms && (cont_tiempo==TIEMPO_TE))
            {
                //toggleLED();
                cont_tiempo=0;
                finTe();
                activarAlarma();
                state = ALARMA;
            }
            break;
        
        case ESPERANDO_CAFE:
            if(trans100ms && (cont_tiempo<TIEMPO_CAFE))
            {
                //toggleLED();
                cont_tiempo++;
            }
            else if(trans100ms &&(cont_tiempo==TIEMPO_CAFE))
            {
                //toggleLED();
                cont_tiempo=0;
                finCafe();
                activarAlarma();
                state = ALARMA;
            }
            break;
        
        case ALARMA:
            if(trans100ms && (cont_tiempo<20))
            {
                cont_tiempo++;
            }
            else if(trans100ms && (cont_tiempo==20))
            {
                apagarAlarma();
                state = REPOSO;
            }
            break;
        
        default:
            break;
        }

        clearVariables();
}