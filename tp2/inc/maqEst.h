#ifndef _MAQEST_H_
#define _MAQEST_H_

#include <stdint.h>

#define REPOSO 0
#define ESPERANDO_DECISION 1
#define ESPERANDO_TE 2
#define ESPERANDO_CAFE 3
#define FINALIZANDO 4
#define ALARMA 5

void clearVariables(void);
void init_maqEst(void);
void ev100ms(void);
void evIngreso_Ficha(void);
void evElige_Te(void);
void evElige_Cafe(void);
void estadoActual(void);

#endif