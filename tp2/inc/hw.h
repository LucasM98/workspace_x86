#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc 
#define INGRESO_FICHA  49  // ASCII para la tecla 1 
#define ELIGE_TE  50  // ASCII para la tecla 2 
#define ELIGE_CAFE  51 // ASCII para la tecla 3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void devolucionFicha(void);
void erogarTe(void);
void erogarCafe(void);
void finTe(void);
void finCafe(void);
void activarAlarma(void);
void apagarAlarma(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
